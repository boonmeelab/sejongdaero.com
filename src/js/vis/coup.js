var margin = {top: 30, right: 150, bottom: 160, left: 200},
    width = 800 - margin.left - margin.right,
    height = 600 - margin.top - margin.bottom;

var x = d3.scale.linear()
    .range([0, width]);
var y = d3.scale.ordinal()
    .rangePoints([0, height], .8);

var xAxis = d3.svg.axis()
    .scale(x)
    .tickSize(-height)
    .orient("top");
var yAxis = d3.svg.axis()
    .scale(y)
    .tickSize(0)
    .orient("left");

var svg = d3.select("#vis").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-15, 1])
svg.call(tip);

var election;
d3.tsv("../public/data/election.tsv", typeElection, function(error, data) {
  election = $.extend(true, [], data); //deep copy
});
d3.tsv("../public/data/coup.tsv", typeCoup, function(error, data) {
  //find periods between coups
  var data_period = $.extend(true, [], data); //deep copy
  data_period.splice(0, 0, {'name':'', 'time':msToYears(new Date().getTime()), 'by':'', 'to':''});
  for(var i = 0; i < data_period.length-1; i++) {
    data_period[i].time -= data_period[i+1].time;
    data_period[i].name = 'หลัง'+data_period[i+1].name;
  }
  //from Siamese revolution of 1932
  data_period[data_period.length-1].time -= getYearsFromString("2475-06-24");
  data_period[data_period.length-1].name = 'หลัง 2475';

  //find average
  var avg_period = 0;
  var max_period = 0;
  var min_period = 100; //in years
  for(var i = 1; i < data_period.length; i++) {
    avg_period += data_period[i].time;
    max_period = Math.max(max_period, data_period[i].time);
    min_period = Math.min(min_period, data_period[i].time);
  }
  avg_period /= data.length;

  //counter numbers
  $('#counter').prepend(Math.floor(data_period[0].time) + "<span class='counter-unit'> ปี </span>" + fractionalInMonths(data_period[0].time) + "<span class='counter-unit'> เดือน </span>" + fractionalInDays(data_period[0].time) + "<span class='counter-unit'> วัน</span>");
  $('#counter_avg').prepend(Math.floor(avg_period) + " ปี " + fractionalInMonths(avg_period) + " เดือน " + fractionalInDays(avg_period) + " วัน ");
  $('#counter_max').prepend(Math.floor(max_period) + " ปี " + fractionalInMonths(max_period) + " เดือน " + fractionalInDays(max_period) + " วัน ");
  $('#counter_min').prepend(Math.floor(min_period) + " ปี " + fractionalInMonths(min_period) + " เดือน " + fractionalInDays(min_period) + " วัน ");

  //drawing
  x.domain(d3.extent(data_period, function(d) { return d.time; })).nice();
  y.domain(data_period.map(function(d) { return d.name; }));

  xAxis.ticks(max_period);
  svg.append("g")
      .attr("class", "x axis")
      .call(xAxis)
    .append("text")
      .attr("dx", "-20px")
      .attr("dy", "-10px")
      .style("text-anchor", "end")
      .text("ระยะเวลา (ปี)");
  svg.selectAll(".tick text")
    .attr("y", "-10px");

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .selectAll(".tick text")
      .style("fill", "#666")
      .style("text-anchor", "end")
      .attr("x", "-20px")
      .attr("y", "2px");

  //average line
  svg.append("g")
      .attr("class", "y avg")
    .append("line")
      .attr("x1", x(avg_period))
      .attr("x2", x(avg_period))
      .attr("y2", height);

  svg.selectAll(".bar")
      .data(data_period)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(0); })
      .attr("y", function(d) { return y(d.name); })
      .attr("width", function(d) { return x(d.time) - x(0); })
      .attr("height", 4);

  svg.selectAll(".bubble")
      .data(election)
    .enter().append("circle")
      .style("fill", "#9e9a3a")
      .attr("r", 5)
      .attr("cx", function(d) { return x(d.time - d.time_coup_before) - x(0); })
      .attr("cy", function(d) { return y('หลัง'+d.name_coup_before) + 2; })
      .on('mouseover', function(d) {
        tip.html(d.name)
        tip.show();
      })
      .on('mouseout', function() {
        tip.hide();
      });
  svg.selectAll(".bubble")
      .data(data_period)
    .enter().append("circle")
      .style("stroke", "#9e9a3a")
      .style("stroke-width", "4")
      .style("fill", function(d,i) { return (i == 0)? "#9e9a3a" : "#fff"; })
      .attr("id", function(d,i) { return (i == 0)? "blink" : ""; })
      .attr("r", 6)
      .attr("cx", function(d) { return x(d.time) - x(0); })
      .attr("cy", function(d) { return y(d.name) + 2; })
      .on('mouseover', function(d, i) {
        if(i != 0) {
          tip.html(data[i-1].name)
          $('body').css('cursor', 'pointer');
          tip.show();
        }
      })
      .on('mouseout', function() {
        $('body').css('cursor', 'auto');
        tip.hide();
      })
      .on('click', function(d, i) {
        if(i != 0) {
          openCenteredPopup(d.link, "", 800, 600);
        }
      });
  //animate ongoing bubble
  function change() {
    var blink = d3.select("#blink");
    blink.transition().duration(500).delay(100)
      .style("fill", blink.style("fill") == "#9e9a3a" ? "#fff" : "#9e9a3a")
      .each("end", change);
  }
  change();

  svg.selectAll(".coup-by")
      .data(data_period)
    .enter().append("text")
      .attr("class", "coup-by")
      .style("fill", "#9e9a3a")
      .attr("x", function(d) { return x(d.time) - x(0) + 15; })
      .attr("y", function(d) { return y(d.name) - 2; })
      .text(function(d) { return d.by; })
  svg.selectAll(".coup-to")
      .data(data_period)
    .enter().append("text")
      .attr("class", "coup-to")
      .style("fill", "#6998db")
      .attr("x", function(d) { return x(d.time) - x(0) + 15; })
      .attr("y", function(d) { return y(d.name) + 12; })
      .text(function(d) { return d.to; });

  //legend
  var legend = svg.append("g")
      .attr("class", "legend")
      .attr("transform", "translate(-100," + (height + 50) + ")");
  legend.append("rect")
      .attr("class", "bar")
      .attr("width", 50)
      .attr("height", 4);
  legend.append("text")
      .attr("x", 75)
      .attr("dy", ".4em")
      .style("text-anchor", "start")
      .text("ระยะเวลาหลังรัฐประหาร");

  var legend1 = legend.append("g")
    .attr("transform", "translate(0,25)");
  legend1.append("rect")
      .attr("class", "bar")
      .attr("width", 50)
      .attr("height", 4);
  legend1.append("circle")
      .style("stroke", "#9e9a3a")
      .style("stroke-width", "4")
      .style("fill", "#fff")
      .attr("r", 6)
      .attr("cx", 50-2)
      .attr("cy", 2);
  legend1.append("text")
      .attr("x", 75)
      .attr("dy", ".4em")
      .style("text-anchor", "start")
      .text("รัฐประหาร (คลิกเพื่ออ่านรายละเอียดเพิ่มเดิม)");

  var legend2 = legend.append("g")
    .attr("transform", "translate(0,50)");
  legend2.append("rect")
      .attr("class", "bar")
      .attr("width", 50)
      .attr("height", 4);
  legend2.append("circle")
      .style("fill", "#9e9a3a")
      .attr("r", 5)
      .attr("cx", 24)
      .attr("cy", 2);
  legend2.append("text")
      .attr("x", 75)
      .attr("dy", ".4em")
      .style("text-anchor", "start")
      .text("การเลือกตั้ง ส.ส.");

  var legend3 = legend.append("g")
    .attr("transform", "translate(350,0)");
  legend3.append("line")
      .style("stroke", "#ff3300")
      .style("fill", "none")
      .style("shape-rendering", "crispEdges")
      .style("stroke", "#ff3300")
      .style("stroke-dasharray", "10,4")
      .attr("x1", 0)
      .attr("x2", 60)
      .attr("y1", 2)
      .attr("y2", 2);
  legend3.append("text")
      .attr("x", 75)
      .attr("dy", ".4em")
      .style("text-anchor", "start")
      .text("ค่าเฉลี่ยระยะเวลาระหว่างรัฐประหาร");

  var legend4 = legend.append("g")
    .attr("transform", "translate(350,25)");
  legend4.append("text")
      .style("fill", "#9e9a3a")
      .attr("dy", ".4em")
      .text("ชื่อ นามสกุล");
  legend4.append("text")
      .attr("x", 75)
      .attr("dy", ".4em")
      .style("text-anchor", "start")
      .text("ผู้ทำรัฐประหาร");

  var legend5 = legend.append("g")
    .attr("transform", "translate(350,50)");
  legend5.append("text")
      .style("fill", "#6998db")
      .attr("dy", ".4em")
      .text("ชื่อ นามสกุล");
  legend5.append("text")
      .attr("x", 75)
      .attr("dy", ".4em")
      .style("text-anchor", "start")
      .text("ผู้ถูกทำรัฐประหาร");
});

//sharing
function shareFacebook() {
  FB.ui({
    method: 'feed',
    link: location.href,
    picture: location.origin + '/public/images/vis/coup-share.jpg',
    name: 'วงเวียนรัฐประหาร',
    caption: 'โดยทีมงานบุญมีแล็บ',
    description: 'นับตั้งแต่การเปลี่ยนแปลงการปกครองในปี พ.ศ. 2475 มีการรัฐประหารในประเทศไทยแล้วทั้งหมด 12 ครั้ง ระยะเวลาระหว่างรัฐประหารแต่ละครั้งจนถึงปัจจุบันแสดงได้ตามแผนภูมิดังนี้'
  }, function(response){});
}
function shareTwitter() {
  openCenteredPopup(
    'https://twitter.com/intent/tweet?original_referer=http://www.ratchadamnoen.org&text=วงเวียนรัฐประหาร โดยทีมงานบุญมีแล็บ&hashtags=ratchadamnoen&url=http://www.ratchadamnoen.org/vis/coup',
    "Share Ratchadamnoen.org on Twitter", 500, 300);
}

function typeCoup(d) {
  d.time = getYearsFromString(d.time);
  return d;
}
function typeElection(d) {
  d.time = getYearsFromString(d.time);
  d.time_coup_before = getYearsFromString(d.time_coup_before);
  return d;
}
function getYearsFromString(s) {
  var date = new Date(s);
  gregorianToThaiYear(date)
  return msToYears(date.getTime());
}
function gregorianToThaiYear(d) {
  d.setFullYear(d.getFullYear() - 543);
}
function msToYears(s) {
  return s/1000/60/60/24/365;
}
function fractional(f) {
  return f - Math.floor(f);
}
function fractionalInMonths(f) {
  return Math.floor(fractional(f)*365/30);
}
function fractionalInDays(f) {
  return Math.round(fractional(fractional(f)*365/30)*30);
}
function openCenteredPopup(url, title, w, h) { //centered to the current parent window
  // Works with dual-screen
  var windowLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
  var windowTop  = window.screenTop  != undefined ? window.screenTop  : window.screenY;

  var left = ((window.innerWidth  / 2) - (w / 2)) + windowLeft;
  var top  = ((window.innerHeight / 2) - (h / 2)) + windowTop;
  var newWindow = window.open(url, title, 'location=no, menubar=no, resizable=no, status=no, titlebar=no, toolbar=no, scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

  // Puts focus on the newWindow
  if (window.focus) {
      newWindow.focus();
  }
}

//Facebook
window.fbAsyncInit = function() {
  FB.init({
    appId       : 611582772256368, // App ID
    channelUrl  : '/public/channel.html', // Channel File
    status      : true, // check login status
    cookies     : true, // enable cookies to allow the server to access the session
    xfbml       : true  // parse XFBML
  });

  // FB.Event.subscribe('auth.authResponseChange', function(response) {
  //   if (response.status === 'connected') {
  //     // user logged in, do something
  //   } else if (response.status === 'not_authorized') {
  //     FB.login(function(response) {
  //       if (response.authResponse) {
  //         // The person logged into your app
  //       } else {
  //         // The person cancelled the login dialog
  //       }
  //     });
  //
  //   } else {
  //     FB.login(function(response) {
  //       if (response.authResponse) {
  //         // The person logged into your app
  //       } else {
  //         // The person cancelled the login dialog
  //       }
  //     });
  //   }
  // });
};

// Load the Facebook SDK Asynchronously
(function(d){
  var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement('script'); js.id = id; js.async = true;
  js.src = "//connect.facebook.net/en_US/all.js";
  ref.parentNode.insertBefore(js, ref);
}(document));
