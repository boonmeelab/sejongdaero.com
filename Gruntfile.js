module.exports = function(grunt) {

  // Load required plugins
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-stylus');

  // Tasks
  grunt.registerTask('default', ['local']);
  grunt.registerTask('local', ['compile:local']);
  grunt.registerTask('product', ['compile:product']);

  grunt.registerTask('compile:local', ['copy', 'jade', 'stylus']);
  grunt.registerTask('compile:product', ['uglify', 'jade', 'stylus']);


  // Project configuration.
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    copy: {
      local: {
        files: [
          {expand: true, cwd: 'src/js/', src: ['**'], dest: 'public/js/'}
        ]
      }
    },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      js: {
        expand: true,     // Enable dynamic expansion.
        cwd: 'src/js/',      // Src matches are relative to this path.
        src: ['**/*.js'], // Actual pattern(s) to match.
        dest: 'public/js/',   // Destination path prefix.
        ext: '.js',   // Dest filepaths will have this extension.
      },
    },

    jade: {
      compile: {
        options: {
          pretty: true,
          data: {
            debug: false,
            title: "Hello wityu"
          },
          compileDebug: false,
          client: true,
          wrapper: 'amd',
          amd: true
        },
        files: [
          {
          expand: true,     // Enable dynamic expansion.
          cwd: 'src/jade/',      // Src matches are relative to this path.
          src: ['**/*.jade'], // Actual pattern(s) to match.
          dest: 'public/view/',   // Destination path prefix.
          ext: '.js',   // Dest filepaths will have this extension.
          }
        ],
      }
    },

    stylus: {
      compile: {
        files: [
          {
          expand: true,     // Enable dynamic expansion.
          cwd: 'src/stylus/',      // Src matches are relative to this path.
          src: ['**/*.styl'], // Actual pattern(s) to match.
          dest: 'public/css/',   // Destination path prefix.
          ext: '.css',   // Dest filepaths will have this extension.
          }
        ]
      }
    },

    clean: {
      options: {
        // "no-write": true
      },
      local: [
        'public/js',
        'public/css',
        'public/view',
      ]
    },

    watch: {
      options: {
        livereload: true,
      },
      js: {
        files: ['src/js/**/*.js'],
        tasks: ['copy:local']
      },
      view: {
        files: ['src/jade/**/*.jade'],
        tasks: ['jade']
      },
      css: {
        files: ['src/stylus/**/*.styl'],
        tasks: ['stylus']
      }
    },
  });

};
