var express = require('express');
var fs = require('fs');
var path = require('path');
var url = require('url');
var util = require('./util');

var app = express();
var port = process.env.PORT || 1945;

// read config file
var confdir = path.join(__dirname,'../config', (process.env.ENV || 'local') + '.json');
var config = require('./config');
config.read(confdir);
// get version
try {
  config.version = fs.readFileSync('./version', { encoding: 'utf8' }).replace(/(\r\n|\n|\r)/gm, '');
  console.log('version', config.version);
} catch (e) {
  console.log('version none');
}

// Basic site info
app.set('config', config);
app.set('title', config.title);
app.set('baseurl', config.baseurl);
app.enable('case sensitive routing');

// Render engine
app.engine('jade', require('jade').__express);
app.set('view engine', 'jade');
app.set('views', __dirname + '/../src/jade');

// Parser
app.use(express.compress());
app.use(express.cookieParser());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.session(config.session));

// Static files
app.use(express.favicon(__dirname + '/../public/images/favicon.ico'));
app.use(util.version('/public'), express.static(__dirname + '/../public', { maxAge: 7*86400000 }));
app.use('/public', express.static(__dirname + '/../public', { maxAge: 7*86400000 }));

// Location info for view
app.use(function(req, res, next) {
  res.locals.location = {
    protocol: 'http:',
    host: config.baseurl.replace(/^https?:\/\//, ''),
    pathname: req.url
  };
  res.locals.location.href = url.format(res.locals.location);
  next();
});

// register web handler
require('./route').register(app);


// Start server
app.listen(port);
console.log('------- wityu.fm web server -------');
console.log('environment: ' + config.env);
console.log('server starts listening to port ' + port + ' ...');
console.log('press Ctrl+C to stop');
