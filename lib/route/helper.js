var urllib = require('url');
var config = require('../config');
var util = require('../util');

exports.register = function(app) {

  app.locals.version = function(pathname) {
    return util.version(pathname);
  };

  app.locals.linkTo = function(pathname, options) {
    var resolved = '';
    var urlOpts = {
      pathname: pathname
    };
    options = options || {};
    if (typeof options === 'string') options = { host: options };
    if (options.host) {
      if (!options.host || options.host === 'self') urlOpts.host = config.baseurl;
      else urlOpts.host = config.url[options.host];
    }
    if (options.query) {
      urlOpts.query = options.query;
    }

    return urllib.format(urlOpts);
  };

};
